def print_formatted(number):
    space=len(bin(number))-2
    for i in range (1,number+1):
        x=str(i).rjust(space)+oct(i)[2:].rjust(space+1)+hex(i)[2:].upper().rjust(space+1)+bin(i)[2:].rjust(space+1)
        print(x)

if __name__ == '__main__':
    n = int(input())
    print_formatted(n)