def print_rangoli(size):
    alphabet=["a","b","c","d","e","f","g","h","i",
              "j","k","l","m","n","o","p","q","r",
              "s","t","u","v","w","x","y","z"]
    rangoli_letters=alphabet[:size]
    rangoli_letters.reverse()
    helper_list_1=[]
    helper_list_2=[]
    k=0
    for i in range (size):
        dash_constant=size*2-(2*i+2)
        alphabet_string=""
        must_print_alpha=""
        for j in range (2*i+1):
            if j%2==0:
                alphabet_string=alphabet_string+rangoli_letters[k]
                k+=1
            else:
                alphabet_string=alphabet_string+"-"
        helper_list_2.append(alphabet_string)    
        k=0
        if i==0:
            must_print_alpha=helper_list_2[0]
        else:
            must_print_alpha=helper_list_2[i-1]+"-"+helper_list_2[i][::-1]
        must_print="-"*dash_constant+must_print_alpha+"-"*dash_constant
        print(must_print)
        if i<size-1:
            helper_list_1.append(must_print)
    helper_list_1.reverse()
    for i in helper_list_1:
        print(i)                