def repeatedstring(s,n):
    count_s=0
    count_helper=0
    count_final=0
    helperstring=''
    x=len(s)
    y=n//x 
    z=n%x 
    for i in s:
        if i=='a':
            count_s+=1
    helperstring=s[:z]
    for i in helperstring:
        if i=='a':
            count_helper+=1
    count_final=count_s*y+count_helper
    return count_final
    
s="abcac"
n=13

print(repeatedstring(s,n))
