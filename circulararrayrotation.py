def rotator(a,k,queries):
    result=[]
    for i in range (k):
        a.insert(0,a[-1])
        del a[-1]
    for i in queries:
        result.append(a[i])
    return result


a=[3,4,5]
k=2 
queries=[0,1,2] 

print(rotator(a,k,queries))
