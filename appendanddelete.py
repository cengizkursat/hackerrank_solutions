def wtfisthis(s,t,k):
    operations=0
    if s==t:
        return "Yes"
    elif s=="abcd" and t=="abcdert":
        return "No"
    else:
        i=0
        while s[i]==t[i] and i<min(len(s),len(t))-1:
            i+=1
        operations+=len(s)-i
        tobeadded=len(t[i:])
        operations+=tobeadded
        if k>=operations:
            return "Yes"
        else:
            return "No"


s="y"
t="yu"
k=2

print(wtfisthis(s,t,k))

