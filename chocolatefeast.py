def chocolateFeast(n, c, m):
    eaten=0
    eaten+=n//c 
    wrapper=eaten
    while wrapper>=m:
        eaten+=wrapper//m
        wrapper=(wrapper%m)+(wrapper//m)
    return eaten