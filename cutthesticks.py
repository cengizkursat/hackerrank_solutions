def cutthesticks(arr):
    count=0
    listem=[len(arr)]
    while True:
        a=min(arr)
        arr=[i-a for i in arr]
        arr.sort()
        k=arr.count(0)
        arr=arr[k:]
        listem.append(len(arr))
        if len(arr)==0:
            break
    listem.remove(0)
    return listem


arr=[5,4,4,2,2,8]
arr2=[1,2,3,4,3,3,2,1]
print(cutthesticks(arr2))