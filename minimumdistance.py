def minimum(a):
    distances=[]
    for i in range (len(a)):
        for j in range (i+1,len(a)):
            if a[i]==a[j]:
                distances.append(abs(j-i))
    if len(distances)!=0:
        return min(distances)
    else:
        return (-1)

a=[7,1,3,4,1,7]
print(minimum(a))

