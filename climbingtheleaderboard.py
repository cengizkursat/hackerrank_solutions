def alisonhell(scores,alice):
    scores=sorted(list(set(scores)))
    index=0
    rank_list=[]
    n=len(scores)
    for i in alice:
        while(n>index and i>=scores[index]):
            index+=1
        rank_list.append(n+1-index)
    return rank_list


scores=[100,90,90,80,75,60]
alice=[50,65,77,90,102]

print(alisonhell(scores,alice))