initial=input()
initial_list=initial.split()
N=int(initial_list[0])
M=int(initial_list[1])
motif=".|."
greet="WELCOME"
trick_list=[]
mid_constant=(M-len(greet))//2
for i in range (N):
    if i==N//2+1:
        print("-"*mid_constant+greet+"-"*mid_constant)
    elif i<N//2:
        length_of_dash=(M-(2*i+1)*len(motif))//2
        one_row="-"*length_of_dash+(2*i+1)*motif+"-"*length_of_dash
        trick_list.append(one_row)
        print(one_row)
trick_list.reverse()
for i in trick_list:
    print(i)