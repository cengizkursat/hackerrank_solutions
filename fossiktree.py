def growth(n):
    initial=1
    if n==0:
        return initial
    else:
        for i in range (1,n+1):
            if i%2==1:
                initial*=2
            else:
                initial+=1
        return initial

growth(0)
growth(1)
growth(4)

