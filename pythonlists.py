N = int(input())
liste=[]
for i in range(0, N):
    tokens = input().split()

    if tokens[0] == 'insert':
        liste.insert(int(tokens[1]), int(tokens[2]))
    elif tokens[0] == 'print':
        print(liste)
    elif tokens[0] == 'remove':
        liste.remove(int(tokens[1]))
    elif tokens[0] == 'append':
        liste.append(int(tokens[1]))
    elif tokens[0] == 'sort':
        liste.sort()
    elif tokens[0] == 'pop':
        liste.pop()
    elif tokens[0] == 'reverse':
        liste.reverse()